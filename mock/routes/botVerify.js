const express = require('express')
const router = express.Router()
// const bodyParser = require('body-parser')
const request = require('request')
// const cors = require('cors')
const secret = '6LdZIsgUAAAAAMAbODP9LU0MmckOyNr2PnJ5OmYZ'

router.post('/signup', function (req, res) {
  console.log('recaptcha:', req)
  if (!req.body.recaptchaToken) {
    return res.status(400).json({ message: 'recaptchaToken is required' })
  }
  if (!req.body.email || !req.body.password) {
    return res.status(400).json({ message: 'email and password are required' })
  }
  const verifyCaptchaOptions = {
    uri: 'https://www.google.com/recaptcha/api/siteverify',
    json: true,
    form: {
      secret: secret,
      response: req.body.recaptchaToken
    }
  }
  request.post(verifyCaptchaOptions, function (err, response, body) {
    if (err) {
      return res.status(500).json({ message: 'oops, something went wrong on our side' })
    }

    if (!body.success) {
      return res.status(500).json({ message: body['error-codes'].join('.') })
    }

    // Save the user to the database. At this point they have been verified.

    // res.status(201).json(response)
    res.status(201).json({ message: '恭喜 ! 你被認證為人類 !' })
  }
  )
})

module.exports = router
