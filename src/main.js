import 'normalize.css/normalize.css'
import '@/assets/css/app.scss'

import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import router from './router'
import store from './store'
// use axios for http communication
import axios from 'axios'
// import VueAxios from 'vue-axios'
import './setup/setupVueFilters'
import './utils/validators/veeValidator'

Vue.use(axios)
Vue.use(Vuex)

new Vue({
  router,
  store,
  axios,
  render: h => h(App)
}).$mount('#app')
