import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '@/views/About')
    },
    {
      path: '/practice',
      name: 'practice',
      component: () => import(/* webpackChunkName: "practice" */ '@/views/Practice')
    },
    {
      path: '/counter',
      name: 'counter',
      component: () => import(/* webpackChunkName: "counter" */ '@/views/Counter')
    },
    {
      path: '/counterWithState',
      name: 'counterWithState',
      component: () => import(/* webpackChunkName: "CounterWithState" */ '@/views/CounterWithState')
    },
    {
      path: '/search',
      name: 'search',
      component: () => import(/* webpackChunkName: "search" */ '@/views/Search')
    },
    {
      path: '/filterDemo',
      name: 'filterDemo',
      component: () => import(/* webpackChunkName: "filterDemo" */ '@/views/FilterDemo')
    },
    {
      path: '/listDemo',
      name: 'listDemo',
      component: () => import(/* webpackChunkName: "listDemo" */ '@/views/ListDemo')
    },
    {
      path: '/modalDemo',
      name: 'modalDemo',
      component: () => import(/* webpackChunkName: "ModalDemo" */ '@/views/ModalDemo')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import(/* webpackChunkName: "Register" */ '@/views/Register')
    },
    {
      path: '/ajaxDemo',
      name: 'ajaxDemo',
      component: () => import(/* webpackChunkName: "AjaxDemo" */ '@/views/AjaxDemo')
    },
    {
      path: '/refDemo',
      name: 'refDemo',
      component: () => import(/* webpackChunkName: "RefDemo" */ '@/views/RefDemo')
    },
    {
      path: '/carouselDemo',
      name: 'carouselDemo',
      component: () => import(/* webpackChunkName: "CarouselDemo" */ '@/views/CarouselDemo')
    },
    {
      path: '/reCaptchaDemo',
      name: 'reCaptchaDemo',
      component: () => import(/* webpackChunkName: "ReCaptchaDemo" */ '@/views/ReCaptchaDemo')
    }
  ]
})
