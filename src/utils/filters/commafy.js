/**
 * @description 將數字字串轉為具千分位及小數點的數字字串
 * @param {Object} value 需進行 filter 的資料
 */
const commafy = (value) => {
  if (value) {
    return value.toString().replace(/^(-?\d+?)((?:\d{3})+)(?=\.\d+$|$)/, function (all, pre, groupOf3Digital) {
      return pre + groupOf3Digital.replace(/\d{3}/g, ',$&')
    })
  }
}

export default commafy
