/* eslint-disable camelcase */
import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend, localize } from 'vee-validate'

import { required, min, alpha_num, max, numeric, digits } from 'vee-validate/dist/rules'
import zh_TW from 'vee-validate/dist//locale/zh_TW'

const req_msg = '此欄位為必填欄位。'
const format_err_msg = '輸入格式錯誤，請重新輸入。'

// Install rule default message.

// setting localize
localize('zh_TW', zh_TW)

// Install rule customize message.
extend('max', { ...max, message: format_err_msg })
extend('min', { ...min, message: format_err_msg })
extend('required', { ...required, message: req_msg })
extend('alpha_num', { ...alpha_num, message: format_err_msg })
extend('numeric', { ...numeric, message: format_err_msg })
extend('digits', { ...digits, message: format_err_msg })

// import component
Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
